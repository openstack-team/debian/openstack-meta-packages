Source: openstack-meta-packages
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
Standards-Version: 4.1.1
Homepage: https://salsa.debian.org/openstack-team/debian/openstack-meta-packages
Vcs-Browser: https://salsa.debian.org/openstack-team/debian/openstack-meta-packages
Vcs-Git: https://salsa.debian.org/openstack-team/debian/openstack-meta-packages.git

Package: openstack-clients
Architecture: all
Depends:
 python3-aodhclient,
 python3-barbicanclient,
 python3-blazarclient,
 python3-cinderclient,
 python3-cloudkittyclient,
 python3-designateclient,
 python3-glanceclient,
 python3-gnocchiclient,
 python3-heatclient,
 python3-ironicclient,
 python3-keystoneclient,
 python3-magnumclient,
 python3-manilaclient,
 python3-mistralclient,
 python3-neutronclient,
 python3-novaclient,
 python3-octaviaclient,
 python3-openstackclient,
 python3-osc-placement,
 python3-swiftclient,
 python3-troveclient,
 python3-vitrageclient,
 python3-watcherclient,
 python3-zaqarclient,
 python3-zunclient,
 ${misc:Depends},
Description: Metapackage to install all Openstack clients
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to implement
 and massively scalable.
 .
 This metapackage will install all of the Openstack clients available for
 remote controlling block devices (Cinder), Image Service (Glance), Identity
 (Keystone), Compute (Nova), Networking (Neutron) and Object Storage (Swift).

Package: openstack-cloud-identity
Architecture: all
Pre-Depends:
 openstack-cloud-services,
Depends:
 keystone,
 ${misc:Depends},
Description: Metapackage to install an Openstack keystone
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to implement
 and massively scalable.
 .
 This metapackage will install keystone, and is needed because of the orders
 in which things should be configured to install all of openstack at once.

Package: openstack-cloud-services
Architecture: all
Depends:
 dbconfig-common,
 mariadb-server | mysql-server,
 ntp,
 rabbitmq-server,
 ${misc:Depends},
Recommends:
 memcached,
Description: Metapackage to install all Openstack service dependencies
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to implement
 and massively scalable.
 .
 This metapackage will install memcached, rabbitmq and MariaDB servers (or MySQL
 if it is already installed) which are all needed in order to run an Openstack
 cloud service.

Package: openstack-compute-node
Architecture: all
Depends:
 ceilometer-agent-compute,
 neutron-openvswitch-agent,
 nova-compute,
 nova-compute-kvm | nova-compute-qemu,
 novnc,
 ntp,
 openstack-pkg-tools,
 ${misc:Depends},
Pre-Depends:
 mariadb-client | mysql-client,
Description: Metapackage to install an Openstack compute node
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to implement
 and massively scalable.
 .
 This metapackage will install a KVM based compute node which will also run
 Neutron OVS Agent.

Package: openstack-deploy
Architecture: all
Depends:
 e2fsprogs,
 ipcalc,
 openstack-pkg-tools,
 python3-keystoneclient,
 xfsprogs,
 ${misc:Depends},
Description: Tools to deploy OpenStack
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to
 implement and massively scalable.
 .
 This package contains preseeder scripts and other deployment tools to setup an
 OpenStack cloud quickly.

Package: openstack-proxy-node
Architecture: all
Pre-Depends:
 openstack-cloud-identity,
Depends:
 aodh-api,
 aodh-evaluator,
 aodh-notifier,
 ceilometer-agent-central,
 glance,
 heat-api,
 heat-api-cfn,
 heat-engine,
 neutron-dhcp-agent,
 neutron-l3-agent,
 neutron-metadata-agent,
 neutron-plugin-openvswitch,
 neutron-plugin-openvswitch-agent | neutron-openvswitch-agent,
 neutron-server,
 nova-api,
 nova-conductor,
 nova-consoleproxy,
 nova-scheduler,
 openstack-dashboard-apache,
 openstack-pkg-tools,
 placement-api,
 ${misc:Depends},
Description: Metapackage to install an Openstack proxy node
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to implement
 and massively scalable.
 .
 This metapackage will install a proxy node server, or management / API node,
 composed of: Nova-API, Nova-Scheduler, Glance, Keystone, Neutron (using
 OpenVSwitch) RabbitMQ, Memcached, and openstack-dashboard.

Package: openstack-puppet-modules
Architecture: all
Depends:
 puppet-module-aodh,
 puppet-module-arioch-redis,
 puppet-module-barbican,
 puppet-module-camptocamp-openssl,
 puppet-module-ceilometer,
 puppet-module-ceph,
 puppet-module-cinder,
 puppet-module-cloudkitty,
 puppet-module-designate,
 puppet-module-duritong-sysctl,
 puppet-module-glance,
 puppet-module-gnocchi,
 puppet-module-heat,
 puppet-module-horizon,
 puppet-module-ironic,
 puppet-module-joshuabaird-ipaclient,
 puppet-module-keystone,
 puppet-module-magnum,
 puppet-module-manila,
 puppet-module-michaeltchapman-galera,
 puppet-module-neutron,
 puppet-module-nova,
 puppet-module-octavia,
 puppet-module-openstack-extras,
 puppet-module-openstacklib,
 puppet-module-oslo,
 puppet-module-ovn,
 puppet-module-placement,
 puppet-module-puppetlabs-apache (>= 3.1.0),
 puppet-module-puppetlabs-concat,
 puppet-module-puppetlabs-haproxy,
 puppet-module-puppetlabs-ntp,
 puppet-module-puppetlabs-rabbitmq,
 puppet-module-puppetlabs-rsync,
 puppet-module-puppetlabs-tftp,
 puppet-module-puppetlabs-vcsrepo,
 puppet-module-richardc-datacat,
 puppet-module-rodjek-logrotate,
 puppet-module-saz-rsyslog,
 puppet-module-saz-ssh,
 puppet-module-sbitio-monit,
 puppet-module-swift,
 puppet-module-tempest,
 puppet-module-voxpupuli-corosync,
 puppet-module-vswitch,
 ${misc:Depends},
Description: Metapackage to install all OpenStack puppet modules
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to implement
 and massively scalable.
 .
 This metapackage will install all puppet modules needed to deploy an OpenStack
 cloud. This is to install on a puppet-master node, then you can use Hiera to
 compose your roles.

Package: openstack-tempest-ci
Architecture: all
Depends:
 ipmitool,
 lsb-release,
 nmap,
 openstack-clients,
 ${misc:Depends},
Description: validate OpenStack Debian packages using tempest and openstack-deploy
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to
 implement and massively scalable.
 .
 This package is to be used to validate OpenStack packages against the tempest
 functional testing suite. It uses openstack-deploy to install a full OpenStack
 deployment on a single virtual machine running itself on an OpenStack cloud,
 using Qemu as hypervisor. Then the Tempest unit tests will be run.
 .
 Typically, this package is to be installed on a Jenkins machine and it's
 script shall be run on regular basis.

Package: openstack-tempest-ci-live-booter
Architecture: all
Depends:
 apache2,
 apache2-utils,
 bind9-host,
 bind9utils,
 bridge-utils,
 ipcalc,
 isc-dhcp-server,
 live-build,
 pxelinux,
 qemu-kvm,
 qemu-utils,
 tftpd-hpa,
 ${misc:Depends},
Description: configures a server to boot a custom live image to test OpenStack
 The best environment to run OpenStack in order to test it, is a Debian Live
 system, booted over PXE, which we just "reinstall" by performing an (IPMI)
 reboot of the server. However, such setup is complicated, and error-prone.
 This is why this package was designed for: configuring a computer to act as
 PXE/DHCP/apache to boot that 2nd server.
 .
 This package configures the different tools (ie: isc-dhcp-server, tftpd-hpa,
 apache and son on) with a script. Its intention is *not* to be clean, but to
 just do the job of configuring for testing OpenStack. It is *not* to be used
 in production.
 .
 This package also contains the necessary scripts to create the Debian Live
 image using live-build, and the qemu-kvm setup to start a virtual machine
 that will PXE boot using that live image, so it is easy to test the setup
 without actual real hardware.

Package: openstack-toaster
Architecture: all
Pre-Depends:
 openstack-compute-node,
 openstack-proxy-node,
Depends:
 ${misc:Depends},
Description: Metapackage to install all of Openstack services at once
 OpenStack is a reliable cloud infrastructure. Its mission is to produce
 the ubiquitous cloud computing platform that will meet the needs of public
 and private cloud providers regardless of size, by being simple to
 implement and massively scalable.
 .
 This metapackage will install all Openstack server packages to run a cloud
 on a single computer. Later, more computers can join the cloud.
